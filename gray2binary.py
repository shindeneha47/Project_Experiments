#Read a color image and convert it into grayscale and
#binary display it on a single window(without using inbuilt method)

import numpy as np
import cv2
import matplotlib.pyplot as plt

	
def rgb2gray(imgarray):
	grimg = np.dot(imgarray[...,:3],[0.299,0.587,0.144])
	convert = grimg.astype('uint8')
	return convert
	
def rgb2bin(convert):
	a = convert > 128
	print a
	b = a*255
	c = b.astype('uint8')
	return c

img = cv2.imread('messi.jpeg')
imgarray = np.array(img)

convert = rgb2gray(imgarray)
cv2.imwrite('messigray.jpeg',convert)

c = rgb2bin(convert)

plt.subplot(221), plt.imshow(convert, 'gray')
plt.subplot(222), plt.imshow(c,'gray')
plt.show()