#find brightest spot in an image

import numpy as np
import cv2
import argparse

im = cv2.imread('messigray.jpeg')
g = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

a= np.array(g)

b = a.max(),np.unravel_index(a.argmax(),a.shape)
print 'max value and its coordinates:',b

(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(g)
cv2.circle(g, maxLoc, 16, (255, 0, 0), 3)
# display the results
cv2.imshow("image", g)
cv2.waitKey(0)

