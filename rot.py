#Read a grayscale image and rotate to (30,45,90) angles,scale to 128X128,
# 512X512 and crop to 50X50 pixels from center of the image.

import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('messigray.jpeg')

def rotateImage(img,angle):
    row,col= img.shape[:2]
    #center=tuple(np.array([row,col])/2)
    center = (row/2,col/2)
    #c1,c2 = center
    rot_mat = cv2.getRotationMatrix2D(center,angle,1.0)
    new_image = cv2.warpAffine(img, rot_mat, (col,row))
    return new_image

  
#cropping   
row,col= img.shape[:2]   
cropped = img[50:row/2,50:col/2]

#resizing

resized = cv2.resize(img, (128,128), interpolation = cv2.INTER_AREA)

#rotate  
new_image1 = rotateImage(img,30)
new_image2 = rotateImage(img,45)
new_image3 = rotateImage(img,90)

plt.subplot(331), plt.imshow(new_image1, 'gray')
plt.subplot(332), plt.imshow(new_image2, 'gray')
plt.subplot(333), plt.imshow(new_image3, 'gray')
plt.subplot(334), plt.imshow(cropped, 'gray')
plt.subplot(335), plt.imshow(resized, 'gray')

plt.show()