import cv2
import Tkinter as tk
from Tkinter import *
from PIL import Image, ImageTk
from ttk import Frame, Button, Style
from Tkinter import RIGHT, BOTH, RAISED,Frame,Canvas
import numpy as np
import serial
import time


class Gui():
	
	def __init__(self):
		self.cap = None
		self.status = None
		self.count = 0
		self.data = [['whitepixels','blackpixels'],[1,2],[3,4]]
		self.Master()
		self.ser = serial.Serial(port='/dev/ttyACM0', baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)
		
		
	def Master(self):
		root = tk.Tk()
		root.withdraw()
		master = tk.Toplevel()
		self.master = master		
		widthOfWindow = self.master.winfo_screenwidth()
		heightOfWindow = self.master.winfo_screenheight()
		self.master.geometry(str(widthOfWindow) + "x" + str(heightOfWindow))
		self.master.title("My Gui")
		label1 = tk.Label(self.master,bg = "black",fg = "white",text="My Project",font = "Verdana 16 bold")
		label1.place(x= 620,y = 10)
		self.Table(self.master,self.data)
		self.bton()
	
				
	def CVCapture(self):
		if self.cap==None:
			self.cap = cv2.VideoCapture(0)
		for i in range (2):
			ret, frame = self.cap.read()	
		return frame
		
	def StartButton(self):
		
		self.status=True
		self.Timer()
		
	def StopButton(self):
		self.status=None
		self.Timer()
		#~ self.Timer(status)
		print "status changed"
		
		
	def Timer(self):
		
		if self.status==True:
			self.Task()
			self.Timer()
			print "Running Progress"
		else:
			print "Timer has Stopped"
		return	
		
		
		
					
	def Open(self):
		CamPhoto = self.CVCapture()
		CamPhotobw = cv2.cvtColor(CamPhoto, cv2.COLOR_RGB2GRAY)
		ret,th1 = cv2.threshold(CamPhotobw,127,255,cv2.THRESH_BINARY)
		#~ bwImage = np.uint8(CamPhoto>120)*255
		cap1 = Image.fromarray(CamPhoto)
		show = ImageTk.PhotoImage(cap1)
		
		whitepix = np.count_nonzero(th1)
		print whitepix
		totalpix = th1.shape[0]*th1.shape[1]
		blackpix = totalpix - whitepix
		print blackpix
		
		label = tk.Label(self.master, image = show)
		label.image = show
		label.place(x= 10,y = 50)
		self.master.update()
		print "before if"
		
		return whitepix,blackpix
		
		
		
	def bton(self):
		start_img= Image.open("start.jpeg")
		start_img = ImageTk.PhotoImage(start_img)
		self.start_img = start_img
		startbutton = tk.Button(master = self.master,text="Start",image =self.start_img,compound = 'top', command = self.StartButton)
		startbutton.place(x=50, y=600)
		
		stop_img= Image.open("stop.png")
		stop_img = ImageTk.PhotoImage(stop_img)
		self.stop_img = stop_img
		stopbutton = tk.Button(master = self.master,text="Stop",image =self.stop_img,compound = 'top', command = self.StopButton)
		stopbutton.place(x=200, y=600)
		
		quit_img= Image.open("stop.png")
		quit_img = ImageTk.PhotoImage(quit_img)
		self.quit_img = quit_img
		quitbutton = tk.Button(master = self.master,text="Quit",image =self.stop_img,compound = 'top', command = self.master.destroy)
		quitbutton.place(x=1150, y=600)
		
	def Table(self,master,data):
		tableFrame = Frame(master)
		r = len(data)
		c = len(data[0])
		rows = []
		for i in range(r):
			cols = []
			if i == 0:
				backgnd = 'blue'
			else:
				backgnd = 'white'
			for j in range(c):
				e = Label(tableFrame,relief=RIDGE,bg=backgnd,width=10)
				e.grid(row=i, column=j, sticky=NSEW)
				e['text']=data[i][j]
				cols.append(e)
			rows.append(cols)
			tableFrame.place(x= 1000,y=100)
			
	def Task(self):
		self.ser.write('a')
		time.sleep(0.5)
		x = self.Open()
		
		
		
		
		
guiObj = Gui()
