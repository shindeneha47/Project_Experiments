

import numpy as np
import cv2
import matplotlib.pyplot as plt


img = cv2.imread('messi.jpeg')
imgarray = np.array(img)
r = img[:,:,0]
g = img[:,:,1]
b = img[:,:,2]	

def rgb2gray (a,b,c):
	greyimg = ((.2989*a)+(0.5870*b)+(0.1140*c))
	x = greyimg.astype('uint8')
	
	a = imgarray.shape[0]
	b = imgarray.shape[1]
	
	
	R = np.ones([a,b])
	
	
	for i in range (0,a):
		for j in range (0,b):
			if x[i][j] > 128:
				R[i][j] = 1
			else:
				R[i][j] = 0
		
	return R
	

x = rgb2gray(r,g,b)
cv2.imshow('messi',x)
cv2.waitKey(0)

